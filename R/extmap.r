###############################################################################
#' Extensions
#'
#' List available extensions for working with files
#'
#' @param extension file extension
#' @param load.fn function to load data
#' @param load.fn.raw function to load data from raw format
#' @param save.fn function to save data
#' @param save.fn.raw function to save data to raw format
#' @param overwrite flag to overwrite data source if already registered in the list of extensions, \strong{defaults to True}
#'
#' @return None
#'
#' @examples
#'  # register extension
#'  register.extension('abc', load.fn = function(filename, ...) print('Test Load'), save.fn = function(json, filename, ...) print('Test Save') )
#'
#'  # print allregistered data sources
#'  names(extensions())
#'
#' @export
#' @rdname Extensions
###############################################################################
register.extension = function
(
	extension,
	load.fn,
	save.fn,
	load.fn.raw=NULL,
	save.fn.raw=NULL,
	overwrite = TRUE
)
	set.options('rtsstorage.extensions', make.list(tolower(extension), 
		list(
			name = tolower(extension),
			load.fn = load.fn, 
			save.fn = save.fn,
			load.fn.raw = load.fn.raw,
			save.fn.raw = save.fn.raw			
		)
	), overwrite = overwrite)


#' @export
#' @rdname Extensions
extensions = function() ifnull(options()$rtsstorage.extensions, list())

#' @export
#' @rdname Extensions
get.extension = function(extension) {
	exts = extensions()
	ext = exts[[tolower(extension)]]
	if(is.null(ext)) {
		warning('Extension:', extension, ' is not found. Will use default load/save functions. Following are registered extensions:', names(exts))
		extension = 'default'
		ext = exts[[extension]]
	}
	
	if(is.null(ext$load.fn) | is.null(ext$save.fn)) {
		warning('Extension:', extension, ' is missing "load.fn" and/or "save.fn" functions.')
	}
	ext
}


#' @export
#' @rdname Extensions
get.extension.raw = function(extension) {
	ext = get.extension(extension)	
	if(is.null(ext$load.fn.raw) | is.null(ext$save.fn.raw)) {
		warning('Extension:', extension, ' is missing "load.fn.raw" and/or "save.fn.raw" functions.')
	}
	ext
}


# lazy load fns
import.fns = list(
	json.load = call('::', 'jsonlite', 'fromJSON'),
	json.save = call('::', 'jsonlite', 'toJSON'),
	
	toml.load = call('::', 'RcppTOML', 'parseTOML')
)

# load import fn
get.import.fn = function(name) {
	fn = import.fns[[name]]
	if(!is.function(fn)) {		
		fn = load.fn(fn)
		import.fns[[name]] = fn		
	}
	fn
}

# evaluate the import call
load.fn = function(fn) tryCatch(eval.parent(fn), error = function(e) stop(e$message, call. = FALSE))



