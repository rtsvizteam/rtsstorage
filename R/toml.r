###############################################################################
# TOML writer
###############################################################################


# helper function to escape strings and format booleans
help2str = function(x) {
	if(is.character(x))
		paste0('"', gsub('\\"','\\\\"',x), '"')
	else if(is.logical(x))
		tolower(paste(x))
	else
		x
}


# Currently there is not functionality to serialize TOML to string
# [write TOML files? · Issue #17 · eddelbuettel/rcpptoml · GitHub](https://github.com/eddelbuettel/rcpptoml/issues/17)
###############################################################################
#' Serialize TOML to string
#' 
#' Traverse list and create TOML representation
#'
#' @param toml toml object
#' @param out internally used buffer, \strong{defaults to ''}
#' @param offset internally used offset, \strong{defaults to c()}
#'
#' @return string representation of given toml object
#'
#' @export
###############################################################################
toml.write = function(toml, out='', offset=c()) {
	# map list flag and length
	if(length(toml) > 1) {
		map = vapply(toml,function(x) c(is.list(x),length(x)),c(TRUE,0))
		map = map[,order(map[1,]),drop=FALSE]
	} else {
		map = matrix(c(is.list(toml[[1]]),length(toml[[1]])), nrow=2)
		colnames(map) = names(toml)
	}

	# setup
	lnames = colnames(map)
	tab = if(length(offset)>0) '\t' else ''
	if(length(offset)>0)
		if(map[1,1] == 0) # not list
			out = paste0( out, '[', paste(offset,collapse='.'), ']\n' )
	
	
	# array of tables
	if(is.null(lnames)) {
		for(i in 1:ncol(map)) {
			out = paste0( out, '[[', paste(offset,collapse='.'), ']]\n' )
			out = toml.write(toml[[i]], out, c())
		}
	} else { # named elements
		for(i in 1:ncol(map)) {
			if(map[1,i] == 0) { # not list
				if(map[2,i] == 1) { # one element
					out = paste0( out, tab, lnames[i], ' = ', help2str(toml[[lnames[i]]]), '\n' )
				} else { # multiple elements
					out = paste0( out, tab, lnames[i], ' = [', paste(help2str(toml[[lnames[i]]]),collapse=',') ,']\n' )
				}				
			} else  # list			
				out = toml.write(toml[[lnames[i]]], out, c(offset,lnames[i]))					
		}
	}
	out
}
