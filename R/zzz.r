.onLoad <- function(libname, pkgname) {
	# JSON
	register.extension('json',
		load.fn = function(filename, ...) {
			fn = get.import.fn('json.load')
			fn(filename)
		},
		save.fn = function(json, filename, ...) {
			fn = get.import.fn('json.save')
			write(fn(json), filename)
		}
	)

	# TOML
	register.extension('toml',
		load.fn = function(filename, ...) {
			fn = get.import.fn('toml.load')
			fn(filename, escape=FALSE)
		},
		save.fn = function(toml, filename, ...) {
			write(toml.write(toml), filename)
		}	
	)
	
	# Zorro
	register.extension('t6',
		load.fn = function(filename, verbose = TRUE, start=0, end=100, ...) zorro.read(filename, 't6', verbose=verbose, start=start, end=end),
		save.fn = function(data, filename, verbose = TRUE, ...) zorro.write(data, filename, 't6', verbose=verbose)        
	)
	register.extension('t8',
		load.fn = function(filename, verbose = TRUE, start=0, end=100, ...) zorro.read(filename, 't8', verbose=verbose, start=start, end=end),
		save.fn = function(data, filename, verbose = TRUE, ...) zorro.write(data, filename, 't8', verbose=verbose)        
	)

	
	# RDATA
	register.extension('rdata',
		load.fn = function(filename, ...) {
			data = NULL
			load(filename)
			data
		},
		load.fn.raw = function(raw.data, ...) {
			data = NULL
			out = rawConnection(raw.data)
			load(out)
			close(out)
			data
		},		
		save.fn = function(data, filename, ...) {
			save(data, file=filename)
		},
		save.fn.raw = function(data, ...) {		
			out = rawConnection(raw(0), "r+") # start with empty raw vector
			save(data, file=out)
			temp = rawConnectionValue(out)
			close(out)
			temp
		}			
	)

	# RDATAX
	# brotli is same speed as save/load and better compression
	register.extension('rdatax',
		load.fn = function(filename, ...) {
			data = NULL
			tryCatch({
				temp = readBin(filename, raw(), file.info(filename)$size)
				unserialize(brotli::brotli_decompress( temp ))
			}, error=function(e) {load(filename); data})
		},
		load.fn.raw = function(raw.data, ...) {
			tryCatch(unserialize(brotli::brotli_decompress(raw.data)), error=function(e) unserialize(raw.data))
		},
		save.fn = function(data, filename, ...) {
			writeBin(brotli::brotli_compress(serialize(data, NULL), 2), filename)
		},
		save.fn.raw = function(data, ...) {
			brotli::brotli_compress(serialize(data, NULL), 2)
		}					
	)
	


	# Parquet
	#* [Difference between Apache parquet and arrow - Stack Overflow](https://stackoverflow.com/questions/56472727/difference-between-apache-parquet-and-arrow)
	#* [A shallow benchmark of R data frame export/import methods](https://data.nozav.org/post/2019-r-data-frame-benchmark/#fn1)
	#* [arrow/r at master · apache/arrow · GitHub](https://github.com/apache/arrow/tree/master/r)
	#
	#install.packages("arrow", repos = "http://cran.rstudio.com")
	data2arrow = function(d) arrow::Table$create(name = xts::.index(d), as.data.frame(d))
	arrow2data = function(a) xts::xts(as.data.frame(a[, names(a)[-1]]), order.by = as.vector(a$name))
		
	register.extension('parquet',
		load.fn = function(filename, ...) {
			arrow2data( arrow::read_parquet(filename, as_data_frame = FALSE) )
		},
		load.fn.raw = function(raw.data, ...) {
			arrow2data( arrow::read_parquet(raw.data, as_data_frame = FALSE) )
		},
		save.fn = function(data, filename, ...) {
			#write_parquet(data.frame(x = 1:5), tf2, compression = "gzip", compression_level = 5)
			arrow::write_parquet(data2arrow( data ), filename)
		},
		save.fn.raw = function(data, ...) {
			out = arrow::BufferOutputStream$create()
			arrow::write_parquet(data2arrow( data ), out)
			as.raw(arrow::buffer(out))
		}					
	)


	

	
	# CSV
	# date.format date format - "\%Y-\%m-\%d" (defaults)
	# use "\%Y-\%m-\%d \%H:\%M:\%S" for storing intra day data
	register.extension('csv',
		load.fn = function(filename, date.col=NULL, date.format = '%Y-%m-%d', ...) {
			data = rts.load.csv(filename)			
			date.col = ifnull(date.col, colnames(data)[1])
					
			data.table::set(data, j=date.col, value=as.POSIXct(strptime(data[[date.col]], date.format)))
			data.table::as.xts.data.table(data)
		},
		save.fn = function(data, filename, date.format = '%Y-%m-%d', ...) {
			con = file(filename, "w")		
			writeChar('Date', con, eos=NULL)			
			suppressWarnings(utils::write.table(data, sep=',',  
				row.names = format(indexts(data), date.format), 
				col.names = NA, file = con, append = TRUE, quote = FALSE))
			close(con)		
		}	
	)

	
	# DEFAULT
	# [serialization - R: serialize objects to text file and back again - Stack Overflow](https://stackoverflow.com/questions/2258511/r-serialize-objects-to-text-file-and-back-again)
	register.extension('default',
		load.fn = function(filename, ...) {
			unserialize(charToRaw(readChar(filename, file.info(filename)$size)))
		},
		load.fn.raw = function(raw.data, ...) {
			unserialize(raw.data)
		},		
		save.fn = function(data, filename, ...) {
			con = file(filename, "w")		
			write(rawToChar(serialize(data, NULL, ascii=T)), file=con)
			close(con)
		},	
		save.fn.raw = function(data, ...) {
			serialize(data, NULL, ascii=T)
		}			
	)
	
	invisible()
}


.onUnload <- function(libpath) {
	gc() # Force garbage collection of connections
}