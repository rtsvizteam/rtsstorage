# [Zorro Download Page](https://www.zorro-trader.com/download.php)
#
# T1 single-stream tick, .t1 file content
#	DATE	time;	// GMT timestamp, OLE datetime, 0.5 ms precision
#	float fVal;		// positive for ask, negative for bid	
#
# T2 order book data
#  DATE  time; // GMT timestamp
#  float fVal; // price quote, positive for ask and negative for bid
#  float fVol; // volume / size
# T2 two-stream tick, internal format
#	DATE	time;	
#	float fHigh, fLow;	
#
# T6 6-stream tick, .t6 file content
#	DATE	time;			// GMT timestamp of fClose
#	float fHigh, fLow;		// (f1,f2)
#	float fOpen, fClose;	// (f3,f4)	
#	float fVal, fVol; 		// additional data, f.i. spread and volume (f5,f6)
#
# T8 options, futures, FOPs
#	DATE	time;		// or trading class
#	float fAsk, fBid; 	// premium without multiplier (f1,f2)
#	float fVal;			// script dependent - bid volume, IV, delta... (f3)
#	float fVol;			// trade volume, ask volume, or open interest (f4)
#	float fUnl;			// unadjusted underlying price (f5)
#	float fStrike;		// (f6)
#	long	Expiry;		// YYYYMMDD (i7)
#	long	Type;		// PUT, CALL, FUTURE, EUROPEAN, BINARY (s8)
#

	
zorro.formats = list(
		t1 = list(
			n = 2,
			size = 8 + 1*4,
			col = c('date', 'close'),
			read = function(con) c(readBin(con,"double",1,size=8), readBin(con,"double",1,size=4)),
			write = function(con, date, data) {writeBin(date, con, size=8);
                writeBin(data[1], con, size=4)
            }            
		),	
		t2 = list(
			n = 3,
			size = 8 + 2*4,
			col = c('date', 'high', 'low'),
			read = function(con) c(readBin(con,"double",1,size=8), readBin(con,"double",2,size=4)),
			write = function(con, date, data) {writeBin(date, con, size=8);
                writeBin(data[1], con, size=4);writeBin(data[2], con, size=4)
            }                        
		),
		t6 = list(
			n = 7,
			size = 8 + 6*4,
			col = c('date', 'high', 'low', 'open', 'close', 'spread', 'volume'),
			read = function(con) c(readBin(con,"double",1,size=8), readBin(con,"double",6,size=4)),
            write = function(con, date, data) {writeBin(date, con, size=8);
                writeBin(data[1], con, size=4);writeBin(data[2], con, size=4);
                writeBin(data[3], con, size=4);writeBin(data[4], con, size=4);
                writeBin(data[5], con, size=4);writeBin(data[6], con, size=4)
            }
		),
		t8 = list(
			n = 9,
			size = 8 + 6*4 + 2*4,
			col = c('date', 'ask', 'bid', 'bvolume', 'avolume', 'underlying', 'strike', 'expiry', 'type'),
			read = function(con) c(readBin(con,"double",1,size=8), readBin(con,"double",6,size=4), readBin(con,"integer",2,size=4)),
            write = function(con, date, data) {writeBin(date, con, size=8);
                writeBin(data[1], con, size=4);writeBin(data[2], con, size=4);
                writeBin(data[3], con, size=4);writeBin(data[4], con, size=4);
                writeBin(data[5], con, size=4);writeBin(data[6], con, size=4);                
                writeBin(data[7], con, size=4);writeBin(data[8], con, size=4);                
            }
		)				
	)
	
    
	
zorro.read = function(filename, format='t6', verbose = TRUE, start=0, end=100) {
	format = zorro.formats[[format]]
	if(is.null(format)) stop('Format', format, 'is not supported')
	
	n = file.size(filename)
	con = file(filename, "rb")
	on.exit(close(con))
	
	n = n / format$size
	
	start = floor(start); end = floor(end)
	if(start < 0 || start > 99) stop('start should be between 0-99')
	if(end < 1 || end > 100) stop('end should be between 1-100')
	if(start >= end) stop('end should be grater than start')
	
	if(start > 0)
		seek(con, where = floor(n * start/100) * format$size, origin = 'start')
	n = floor(n * end/100) - floor(n * start/100)
	
	
	x = matrix(0,n,format$n)
	colnames(x) = format$col
	for(i in 1:n) {
		if(verbose) if((i %% 100000)==0) cat('Progress', 100*i/n, '\n')	
		x[i,] = format$read(con)
	}
	if(verbose) cat('Done', '\n')	
	
	#xts::xts(x[,-1], as.POSIXct((x[,1]-25569) * 86400, origin = "1970-01-01", tz = "UTC"))
	xts::xts(x[,-1], anytime::anytime((x[,1]-25569) * 86400,'UTC',TRUE))
}


zorro.write = function(data, filename, format='t6', verbose = TRUE) {
	format = zorro.formats[[format]]
	if(is.null(format)) stop('Format', format, 'is not supported')
    
    date = xts::.index(data) / 86400 + 25569
    temp = zoo::coredata(data)
	n = nrow(data)
    
	con = file(filename, "wb")
	on.exit(close(con))
       
	# reverse order
	for(i in n:1) {
		if(verbose) if((i %% 100000)==0) cat('Progress', 100*(n-i)/n, '\n')	
		format$write(con, date[i], temp[i,])
	}
	if(verbose) cat('Done', '\n')	
}


