#' @title `rtsstorage` - R Time Series Universal Data Storage API.
#' 
#' @description The `rtsstorage` package provides abstraction data storage for R Time Series.
#'
#' @importFrom R6 R6Class 
#' 
#' @name rtsstorage
#' @docType package
#' 
NULL

#utils::globalVariables('data', add=FALSE)