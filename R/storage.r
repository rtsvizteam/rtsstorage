###############################################################################
# Storage Models
#
# Define data storage models. I.e. file / database
#
# All models follow the same interface: 
# - load function = load data for given ID
# - save function = save data for given ID
# - location = folder / collection where data is saved
# - exists function = check if data is saved for given ID
# - delete function = delete data for given ID
# - list function = list all IDs
###############################################################################



###############################################################################
#' Default location to save data
#'
#' The following waterfall is used to lookup the default location: 
#' 1. options
#' 2. environment
#' 3. default option
#' 
#' Good practice is not to store this setting inside the script files. 
#' Add options(RTSSTORAGE_FOLDER='C:/Data') line to the .Rprofile to use 'C:/Data' folder.
#'
#' @return default location to save data
#'
#' @examples
#' 	# Default location to save data
#' 	rtsstorage.default.location()
#'
#' @export
#' @rdname DefaultStorageOptions
###############################################################################
rtsstorage.default.location = function() {
	get.default.option( 'RTSSTORAGE_FOLDER', tempdir() )
}


###############################################################################
#' Default database to save data
#'
#' Good practice is not to store this setting inside the script files. 
#' Add options(RTSSTORAGE_DB='mongodb://localhost') line to the .Rprofile to use 'mongodb://localhost' database.
#'
#' @return default database to save data
#'
#' @export
#' @rdname DefaultStorageOptions
###############################################################################
rtsstorage.default.database = function() {
	url = 'mongodb://localhost'
	get.default.option('RTSSTORAGE_DB', url)
}



# Abstract Storage Class
Storage = R6::R6Class("Storage",
public = list(			
	initialize = function() {
		if(R6::getR6Class(self) == "Storage")
			stop(paste0(R6::getR6Class(self), " is an abstract class that can't be initialized."))
	},
	finalize = function() { 
		0
	},

	# load data for given id
	load = function(id) {
		0
	},
	
	# save data for id
	save = function(data, id) {
	},
	
	# check if data for given id exists
	exists = function(id) 0,
	
	# delete data for given id
	delete = function(id) {
		0
	},
	
	# list all saved ids
	list = function() 0,
	
	# generate ticker from id
	ticker = function(id) id
))


###############################################################################
#' MongoDB GridFS Storage model
#'
#' @param url address of the mongodb server in mongo connection string URI format, 
#' \strong{defaults to rtsstorage.default.database database}. 
#' 
#' For local mongodb server, use 'mongodb://localhost' URI.
#' For local authenticated mongodb server, use 'mongodb://user:password@localhost' URI.
#' @param tag tag name, \strong{defaults to 'data1'}
#' @param db name of database, \strong{defaults to 'data_storage'}
#' @param extension file extension, \strong{defaults to 'rdatax'}
#'
#' @return list with storage options
#'
#' @export
###############################################################################
rtsstorage.database.mongo = function
(
	url = rtsstorage.default.database(),
	tag = 'data1',
	db = 'data_storage',
	extension = 'rdatax'
) {
	# connect to db; if db is not setup, mongo will create empty one
	fs = mongolite::gridfs(db = db, url = url)
	
	# check saved files and remove all duplicates
	index = fs$find()$name
	index = index[duplicated(index)]
	
	# remove all duplicates
	if( len(index) > 0 )
		for(i in index)
			fs$remove(i)
		
	MongoDBStorage$new(fs, tag, get.extension.raw(extension))
}

# MongoDB storage definition
MongoDBStorage = R6::R6Class("MongoDBStorage", inherit = Storage,
public = list(
	fs = NULL,
	location = NULL,	
	ext = NULL,
		
	initialize = function(fs, location = '', ext = NULL) {
		self$fs = fs
		self$location = location		
		self$ext = ext
	},
	finalize = function() self$fs$disconnect(),

	load = function(id) {
		i = self$ticker(id)
		# original - unserialize(self$fs$read( i )$data),
		temp = self$fs$read( i )$data		
		self$ext$load.fn.raw(temp)
	},
	
	save = function(data, id) {
		i = self$ticker(id)
		# the only way to update data is to remove the old file
		try(self$fs$remove(i), silent = TRUE)
		# original - s$fs$write(serialize(data, NULL), i)
		self$fs$write( self$ext$save.fn.raw(data), i)
	},
	
	exists = function(id) sum(self$fs$find()$name == self$ticker(id)) > 0,
	
	delete = function(id) {
		i = self$ticker(id)
		try(self$fs$remove(i), silent = TRUE)
	},
	
	list = function() self$fs$find()$name,
	
	ticker = function(id) paste0(self$location, '|', id)
))





# [thor • thor](https://richfitz.github.io/thor/articles/thor.html)
#provides an wrapper around LMBD; the lightning memory-mapped database

###############################################################################
#' LMDB Storage model
#'
#' @param location storage location, \strong{defaults to rtsstorage.default.location folder}
#' @param db filename of database, \strong{defaults to 'data_storage'}
#' @param tag tag name, \strong{defaults to 'data1'}
#' @param extension file extension, \strong{defaults to 'rdatax'}
#' @param mapsize mapsize, \strong{defaults to 104857600}. Please increase mapsize if you get
#' 	Error MDB_MAP_FULL: Environment mapsize limit reached: mdb_put (code: -30792)	
#'
#' @return list with storage options
#'
#' @export
###############################################################################
rtsstorage.database.lmdb = function
(
	location = rtsstorage.default.location(),
	db = 'data_storage'	,
	tag = 'data1',
	extension = 'rdatax',
	mapsize=104857600	
) {
	# connect to db; if db is not setup, thor will create empty one
	con =  thor::mdb_env(file.path(location,paste0(db,'.lmdb')), mapsize=mapsize)
	
	LMDBStorage$new(con, tag, get.extension.raw(extension))
}

# LMDB storage definition
LMDBStorage = R6::R6Class("LMDBStorage", inherit = Storage,
public = list(
	con = NULL,
	location = NULL,
	ext = NULL,
		
	initialize = function(con, location = '', ext = NULL) {
		self$con = con
		self$location = location
		self$ext = ext
	},
	finalize = function() {},

	load = function(id) {
		i = self$ticker(id)		
		temp = self$con$get(i)
		self$ext$load.fn.raw(temp)
	},
	
	save = function(data, id) {
		i = self$ticker(id)
		self$con$put(i, self$ext$save.fn.raw(data) )
	},
	
	exists = function(id) self$con$exists( self$ticker(id) ),
		
	delete = function(id) {
		i = self$ticker(id)		
		try(self$con$del(i), silent = TRUE)
	},
	
	list = function() self$con$list(),
	
	ticker = function(id) paste0(self$location, '|', id)
))



	

	
# [Storing R Objects as a SQLite Blob · GitHub](https://gist.github.com/jfaganUK/5a6ae4c2be54e45973f1)
# [filehashsqlite/filehash-SQLite.R at master · rdpeng/filehashsqlite · GitHub](https://github.com/rdpeng/filehashsqlite/blob/master/R/filehash-SQLite.R)
# [large blob · Issue #192 · r-dbi/RSQLite · GitHub](https://github.com/r-dbi/RSQLite/issues/192)

# [Writing SQL that works on PostgreSQL, MySQL and SQLite](https://evertpot.com/writing-sql-for-postgres-mysql-sqlite/)
# [Converting MySQL to PostgreSQL - Wikibooks, open books for an open world](https://en.wikibooks.org/wiki/Converting_MySQL_to_PostgreSQL)



###############################################################################
#' SQLite Storage model
#'
#' @param location storage location, \strong{defaults to rtsstorage.default.location folder}
#' @param db filename of database, \strong{defaults to 'data_storage'}
#' @param tag tag name, \strong{defaults to 'data1'}
#' @param extension file extension, \strong{defaults to 'rdatax'}
#'
#' @return list with storage options
#'
#' @export
###############################################################################
rtsstorage.database.sqlite = function
(
	location = rtsstorage.default.location(),
	db = 'data_storage'	,
	tag = 'data1',
	extension = 'rdatax'
) {
	# connect to db; if db is not setup, sqlite will create empty one
	con = RSQLite::dbConnect(RSQLite::SQLite(), file.path(location,paste0(db,'.sqlite')))

	# The table has two columns, an *id* column and a column called *graph* which is a **blob** type. This type just stores binary data.
	RSQLite::dbExecute(con, 'CREATE TABLE IF NOT EXISTS storage 
                 (id TEXT PRIMARY KEY,
                  data BLOB)')
	
	SQLiteDBStorage$new(con, tag, get.extension.raw(extension))
}
  

# SQLite storage definition
SQLiteDBStorage = R6::R6Class("SQLiteDBStorage", inherit = Storage,
public = list(
	con = NULL,
	location = NULL,
	ext = NULL,
		
	initialize = function(con, location = '', ext = NULL) {
		self$con = con
		self$location = location
		self$ext = ext
	},
	finalize = function() RSQLite::dbDisconnect(self$con),

	
	load = function(id) {
		i = self$ticker(id)
		temp = RSQLite::dbGetQuery(self$con, 'SELECT data FROM storage WHERE id = ?', list(i))		
		self$ext$load.fn.raw(temp$data[[1]])
	},
	
	save = function(data, id) {
		i = self$ticker(id)
		RSQLite::dbExecute(self$con, 'REPLACE INTO storage (id, data) VALUES (?, ?)', list(i, list( self$ext$save.fn.raw(data) )  ))
	},
	
	exists = function(id) sum(self$list() == self$ticker(id)) > 0,
	
	delete = function(id) {
		i = self$ticker(id)		
		try(RSQLite::dbExecute(self$con, 'DELETE FROM storage WHERE id = ?', list(i)), silent = TRUE)
	},
	
	list = function() RSQLite::dbGetQuery(self$con, 'SELECT id FROM storage'),
	
	ticker = function(id) paste0(self$location, '|', id)
))


###############################################################################
#' File Storage model
#'
#' @param location storage location, \strong{defaults to rtsstorage.default.location folder}
#' @param extension file extension, \strong{defaults to 'rdata'}
#' @param tag tag name, \strong{defaults to 'data1'}
#' @param custom.folder custom folder flag, \strong{defaults to False}
#' 	if flag is False \strong{default}, the data is stored at the \code{"location\tag_extnsion"} folder.
#'	if flag is True, the data is stored at the \strong{location} folder.
#' @param ... additional parameters to pass to save / load functions
#'
#' @return list with storage options
#'
#' @export
###############################################################################
rtsstorage.file = function
(
	location = rtsstorage.default.location(),
	extension = 'rdata',
	tag = 'data1',
	custom.folder = FALSE,
	...
) {
	if(nchar(trim(location))==0) location='.'
	if(!dir.exists(location)) dir.create(location, TRUE, TRUE) 	

	if(!custom.folder) location = file.path(location, paste0(tag, '_', extension)) 
	
	if(!dir.exists(location)) dir.create(location, TRUE, TRUE) 	
		
	FileStorage$new(location, get.extension(extension), ...)
}


# File rdata storage definition
FileStorage = R6::R6Class("FileStorage", inherit = Storage,
public = list(
	location = NULL,
	ext = NULL,	
		
	initialize = function(location, ext, ...) {			
		self$location = location
		self$ext = ext
	},
	# finalize = function() { }

	load = function(id) { 
		filename = self$ticker(id)
		self$ext$load.fn(filename)
	},
	
	save = function(data, id) {
		filename = self$ticker(id)
		self$ext$save.fn(data, filename)
	},
	
	exists = function(id) file.exists(self$ticker(id)),

	delete = function(id) {
		i = self$ticker(id)
		try(unlink(i, TRUE, TRUE), silent = TRUE)
	},
	
	list = function() list.files(self$location, pattern = paste0('\\.',self$ext$name,'$'), ignore.case = T),
	
	ticker = function(id) file.path(self$location, paste0(id, '.', self$ext$name))		
))
