rtsstorage
====


R Time Series Universal Data Storage API
===


Testing functionality of the `rtsstorage` package.


Example :
===

```R
	# load historical stock prices
	library(quantmod)
	
	# tickers to load data
	env = new.env()
	Symbols = c('spy','aapl','ibm','DBC','EEM','EWJ','GLD')
	
	# download data
	getSymbols(Symbols, env, src = 'yahoo', from = '1900-01-01', verbose=TRUE)
	

	# save(env, file='temp.data.rdata')
	library(quantmod)
	load('temp.data.rdata')
	
	
	# load `rtsstorage` package
	library(rtsstorage)

	# default locations for file/database storage
	rtsstorage.default.location()
	rtsstorage.default.database()
	
	# currently registered file formats
	names(extensions())
	
	
	#----------------------------
	# CSV file storage
	#----------------------------
	stg = rtsstorage.file('temp123','csv','yahoo')
	
	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# list available tickers
	stg$list()
	
	# load data
	head(stg$load('spy'))
	
	# delete data
	stg$delete('spy')


	#----------------------------
	# Rdata file storage
	#----------------------------	
	stg = rtsstorage.file('temp123','rdata','yahoo')
	
	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# list available tickers
	stg$list()
	
	# load data
	head(stg$load('spy'))
	
	# delete data
	stg$delete('spy')
	
	#----------------------------
	# Rdatax file storage
	#----------------------------	
	stg = rtsstorage.file('temp123','rdatax','yahoo')
	
	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# list available tickers
	stg$list()

	#----------------------------
	# parquet file storage
	#----------------------------	
	stg = rtsstorage.file('temp123','parquet','yahoo')
	
	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# load data
	head(stg$load('spy'))
	
	
	# list available tickers
	stg$list()

	
	# remove folder
	unlink('temp123', TRUE, TRUE)
	

	
	#----------------------------
	# MongoDB file storage
	#----------------------------
	stg = rtsstorage.database.mongo(tag='yahoo') # rdatax
	stg = rtsstorage.database.mongo(tag='yahoo', extension='parquet')
	
	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# list available tickers
	stg$list()
	
	# load data
	head(stg$load('SPY'))
	
	# delete data
	stg$delete('SPY')
	
	# delete all
	for(s in ls(env)) stg$delete(s)
	

	#----------------------------
	# lmdb file storage	
	#----------------------------
	library(rtsstorage)	
	stg = rtsstorage.database.lmdb('temp123','temp')

	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# list available tickers
	stg$list()
	
	# load data
	head(stg$load('SPY'))
	
	# delete data
	stg$delete('SPY')
	
	stg$list()
		
		
	
	#----------------------------
	# sqlite file storage
	#----------------------------
	# load `rtsstorage` package
	library(rtsstorage)	
	stg = rtsstorage.database.sqlite('temp123','temp')

		
	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# list available tickers
	stg$list()
	
	# load data
	head(stg$load('SPY'))
	
	# delete data
	stg$delete('SPY')
	
	stg$list()

```	
	