rtsstorage
====


R Time Series Universal Data Storage API
===


Example of sharing data between R and Python in Apache Parquet format.


Installation:
===


You can either use the inst\python\ptsstorage.py file included with 'rtsstorage' package or install
accompanying python 'ptsstorage' package with following code:


```
pip install git+ssh://git@bitbucket.org/rtsvizteam/ptsstorage.git
```

	

Example :
===

```R
	library(quantmod)
	load('temp.data.rdata')
	
	
	# load `rtsstorage` package
	library(rtsstorage)

	#----------------------------
	# parquet file storage
	#----------------------------	
	stg = rtsstorage.file('c:\\temp','parquet','yahoo')
	
	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# load data
	head(stg$load('spy'))
		
	# list available tickers
	stg$list()

	
	
	
	
	#----------------------------
	# MongoDB file storage
	#----------------------------
	stg = rtsstorage.database.mongo(tag='yahoo', extension='parquet')
	
	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# list available tickers
	stg$list()
	
	# load data
	head(stg$load('SPY'))
	
	# delete data
	stg$delete('SPY')
	
```

Access stored data in python:

```python
	import ptsstorage as pts

	# -------------------------
	# Example File Storage
	# assume 'rtsstorage' package save data at 'c:\temp\yahoo_parquet' folder
	# -------------------------
	stg = pts.FileStorage(r'c:\temp\yahoo_parquet')
	stg.list()
	stg.exists('ibm')

	a = stg.load('ibm')
	stg.save(a,'ibm1')
	stg.list()

	stg.delete('ibm1')
	stg.list()

	del stg


	# -------------------------
	# Example Database Storage
	# assume 'rtsstorage' package save data at 'mongodb://localhost' mongodb under 'data_storage' label
	# -------------------------
	stg = pts.MongoDBStorage('mongodb://localhost', 'data_storage')
	stg.list()
	stg.exists('IBM')

	a = stg.load('IBM')
	stg.save(a,'IBM1')
	stg.list()


	stg.delete('IBM1')
	stg.list()
	
	del stg	
		
```



