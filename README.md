rtsstorage
====


R Time Series Universal Data Storage API
===


The `rtsstorage` package provides abstraction data storage for R Time Series.


Installation:
===


To install the development version run following code:


```r
remotes::install_bitbucket("rtsvizteam/rtsstorage")
```
	

Example :
===

```R
	# load historical stock prices
	library(quantmod)
	
	# tickers to load data
	env = new.env()
	Symbols = c('spy','aapl','ibm','DBC','EEM','EWJ','GLD')
	
	# download data
	getSymbols(Symbols, env, src = 'yahoo', from = '2018-01-01', to = '2018-02-13', verbose=TRUE)
	
	
	# load `rtsstorage` package
	library(rtsstorage)

	# default locations for file/database storage
	rtsstorage.default.location()
	rtsstorage.default.database()
	
	# currently registered file formats
	names(extensions())
	
	
	#----------------------------
	# CSV file storage
	#----------------------------
	stg = rtsstorage.file('temp123','csv','yahoo')
	
	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# list available tickers
	stg$list()
	
	# load data
	head(stg$load('spy'))
	
	# delete data
	stg$delete('spy')


	#----------------------------
	# Rdata file storage
	#----------------------------	
	stg = rtsstorage.file('temp123','rdata','yahoo')
	
	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# list available tickers
	stg$list()
	
	# load data
	head(stg$load('spy'))
	
	# delete data
	stg$delete('spy')
	
	# remove folder
	unlink('temp123', TRUE, TRUE)
	
```


Example : use MongoDB storage
===

If you do not have MongoDB installed, the good tutorial to start using MongoDB on Windows: [Install, setup and start MongoDB on Windows](https://blog.ajduke.in/2013/04/10/install-setup-and-start-mongodb-on-windows/)
	
* Create database folder
	rm -f -r c:\mongodb\data
	mkdir c:\mongodb\data
* Start MongoDB server	
	`mongod.exe --dbpath "c:\mongodb\data"`
* Test MongoDB setup
	`mongo.exe`

```
	show dbs
	# In the clean install, you expected to see 
	# admin  0.000GB
	# local  0.000GB

	use data_storage
	show collections
```



```R
	# save(env, file='temp.data.rdata')
	library(quantmod)
	load('temp.data.rdata')
	
	
	# load `rtsstorage` package
	library(rtsstorage)

	# default locations for file/database storage
	rtsstorage.default.location()
	rtsstorage.default.database()
	
	# currently registered file formats
	names(extensions())
	
	
	#----------------------------
	# MongoDB file storage
	#----------------------------
	stg = rtsstorage.database.mongo(tag='yahoo') # rdatax
	stg = rtsstorage.database.mongo(tag='yahoo', extension='parquet')
	
	# save data
	for(s in ls(env)) stg$save(env[[s]], s)
	
	# list available tickers
	stg$list()
	
	# load data
	head(stg$load('SPY'))
	
	# delete data
	stg$delete('SPY')
	
	# delete all
	for(s in ls(env)) stg$delete(s)
	

```

The R Time Series Universal Data Storage API makes it really easy to move data across different formats.
For any two supported storage models following code will migrate data from 'source' storage to 
'destination' storage.

```R
	# load `rtsstorage` package
	library(rtsstorage)
	
	
	#----------------------------
	# 'source' storage - files in 'rdata' format
	#----------------------------
	src = rtsstorage.file('temp123','rdata','yahoo')

	#----------------------------
	# 'destination' storage - files in 'parquet' in MongoDB db
	#----------------------------
	dest = rtsstorage.database.mongo(tag='yahoo', extension='parquet')

	#----------------------------
	# optionally delete all from 'destination' storage 
	#----------------------------
	for( item in dest$list() ) dest$delete(item)
	
	#----------------------------
	# move data from 'source' storage to 'destination' storage
	#----------------------------
	for( item in src$list() ) dest$save(src$load(item), item)
		
```
	

	
To-do: Consider other storage formats
===

* [feather](https://blog.rstudio.com/2016/03/29/feather/)
* [fst](https://github.com/fstpackage/fst)

